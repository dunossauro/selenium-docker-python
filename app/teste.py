from selenium.webdriver import Remote
from sys import argv

browser = Remote(
    command_executor=f'http://{argv[-1]}:4444/wd/hub',
    desired_capabilities={'browserName': argv[-1]}
)

browser.get('http://ddg.gg')

print(browser.page_source)

browser.quit()
